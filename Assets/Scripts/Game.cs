﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class Game
{
	public static Game current;
	public static string name1;
	public static string name2;
	public static string name3;
	public static string name4;
	public static string name5;
	public static int level1;
	public static int level2;
	public static int level3;
	public static int level4;
	public static int level5;
	public static int difficulty;
	public static int level;
	public static string sType;
	public static string sType2;
	public static bool isgun;
	public static int player1health;
	public static int lives;
	public static bool canon;


	public static void NeedGameUpdate ()
	{

		difficulty = GameController.Instance.realDifficulty;
		level = GameController.Instance.currentLevel;
		sType = GameController.Instance.Type;
		sType2 = GameController.Instance.Type2;
		isgun = Player.gun;
		lives = Player.lives;
		Player.setHealth2 ();


		
	}
	public static void NeedUpdate2 ()
	{
		Player.canon = canon;
	}
	public static void NeedGameUpdate2 ()
	{

		GameController.Instance.realDifficulty = difficulty;
		GameController.Instance.currentLevel = level;
		GameController.Instance.Type = sType;
		GameController.Instance.Type2 = sType2;
		Player.setLives ();
		Player.gun = isgun;
		Player.setHealth ();
	}
	public static void SaveCanonData ()
	{
		using (var writer = new BinaryWriter(File.OpenWrite("canon"))) {
			writer.Write (canon);
			writer.Close ();
		}
		Debug.Log ("canon"+canon);
	}


	public static void SaveGameData ()
	{
		using (var writer = new BinaryWriter(File.OpenWrite("Level"))) {
			writer.Write (level);
			writer.Close ();
		}
		using (var writer = new BinaryWriter(File.OpenWrite("Type"))) {
			writer.Write (sType);
			writer.Close ();
		}
		using (var writer = new BinaryWriter(File.OpenWrite("Type2"))) {
			writer.Write (sType2);
			writer.Close ();
		}
		using (var writer = new BinaryWriter(File.OpenWrite("gun"))) {
			writer.Write (isgun);
			writer.Close ();
		}

		using (var writer = new BinaryWriter(File.OpenWrite("Difficultee"))) {
			writer.Write (difficulty);
			writer.Close ();
		}
		using (var writer = new BinaryWriter(File.OpenWrite("Player1Health"))) {
			writer.Write (player1health);
			writer.Close ();
		}

		using (var writer = new BinaryWriter(File.OpenWrite("lives"))) {
			writer.Write (lives);
			writer.Close ();
		}

		Debug.Log ("level"+level);
		Debug.Log ("type"+sType);
		Debug.Log ("type2"+sType2);
		Debug.Log ("gun"+isgun);
		Debug.Log ("diff"+difficulty);
		Debug.Log ("health"+player1health);
		Debug.Log ("lives"+lives);
	}

	public static void LoadGameData ()
	{
		using (var reader = new BinaryReader(File.OpenRead("Level"))) {
			level = reader.ReadInt32 ();
			reader.Close ();
		}
		using (var reader = new BinaryReader(File.OpenRead("Difficultee"))) {
			difficulty = reader.ReadInt32 ();
			reader.Close ();
		}
		using (var reader = new BinaryReader(File.OpenRead("Type"))) {
			sType = reader.ReadString ();
			reader.Close ();
		}
		using (var reader = new BinaryReader(File.OpenRead("Type2"))) {
			sType2 = reader.ReadString ();
			reader.Close ();
		}
		using (var reader = new BinaryReader(File.OpenRead("gun"))) {
			isgun = reader.ReadBoolean ();
			reader.Close ();
		}

		using (var reader = new BinaryReader(File.OpenRead("Player1Health"))) {
			player1health = reader.ReadInt32 ();
			reader.Close ();
		}

		using (var reader = new BinaryReader(File.OpenRead("lives"))) {
			lives = reader.ReadInt32 ();
			reader.Close ();
		}
		Debug.Log (level);
		Debug.Log (sType);
		Debug.Log (sType2);
		Debug.Log (isgun);
		Debug.Log (difficulty);
		Debug.Log (player1health);
		
	}
	public static void LoadCanonData ()
	{
		using (var reader = new BinaryReader(File.OpenRead("canon"))) {
			canon = reader.ReadBoolean ();
			reader.Close ();
		}
		Debug.Log ("canon"+canon);
	}




}
