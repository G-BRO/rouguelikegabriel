﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

	public static GameController Instance;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 100;
	public int faceShot;
	public string Buying = "null";
	public bool hasGameStarted = false;
	public Rigidbody2D orbPrefab;
	public AudioClip gameOverSound;
	public bool invincible;
	public int Difficulty;
	public int realDifficulty;
	public bool isHard;
	private InputField nameInput;
	public string pname1;
	public string pname2;
	public string pname3;
	public string pname4;
	public string pname5;
	public string pnamenew;
	public int plevel1;
	public int plevel2;
	public int plevel3;
	public int plevel4;
	public int plevel5;
	private List<Enemy> enemies;
	private GameObject startScreen;
	private GameObject controlScreen;
	private GameObject charScreen;
	private GameObject charScreen2;
	private GameObject diffScreen;
	private GameObject pickups;
	private static GameObject buyScreen;
	private GameObject leadScreen;
	private Text titleText;
	private bool gameStart = false;
	private GameObject Archer;
	private GameObject Wizard;
	private GameObject Drag;
	private GameObject Spear;
	private GameObject Norm;
	private GameObject NormGuy;
	public string Type = "";
	private GameObject hardEnemies;
	public string Type2 = "";
	
	private Text leadText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	public int currentLevel = 1;

	void Awake ()
	{
		if (Instance != null && Instance != this) {
			DestroyImmediate (gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad (gameObject);
		enemies = new List<Enemy> ();
	}
	
	void Start ()
	{
		InitializeGame ();
	}

	private void InitializeGame ()
	{
	
		settingUpGame = true;
		startScreen = GameObject.Find ("Start Screen");
		controlScreen = GameObject.Find ("Control Screen");
		charScreen = GameObject.Find ("Char Screen 1");
		charScreen2 = GameObject.Find ("Char Screen 2");
		diffScreen = GameObject.Find ("Diff Screen");
		buyScreen = GameObject.Find ("Shop Screen");
		hardEnemies = GameObject.Find ("Hard Enemies");
		pickups = GameObject.Find ("pickups");
		leadScreen = GameObject.Find ("Leader Screen");
		
		leadText = GameObject.Find ("Leader Text").GetComponent<Text> ();


		Wizard = GameObject.Find ("Player Wiz");
		Spear = GameObject.Find ("Player Spear");
		Norm = GameObject.Find ("Player Norm");
		NormGuy = GameObject.Find ("Player NormGuy");
		Archer = GameObject.Find ("Player");
		Drag = GameObject.Find ("Player Drag");
		 

		DontDestroyOnLoad (buyScreen);
	

		if (gameStart == false) {
			startScreen.SetActive (true);
			buyScreen.SetActive (false);
			controlScreen.SetActive (false);
			leadScreen.SetActive (false);
		
			gameStart = true;
		} else {
			startScreen.SetActive (false);
			charScreen.SetActive (false);
			diffScreen.SetActive (false);
			buyScreen.SetActive (false);
			controlScreen.SetActive (false);
			leadScreen.SetActive (false);


				

		


			if (Type == "wizard") {
				Archer.SetActive (false);
				Drag.SetActive (false);
				Wizard.SetActive (true);
				Norm.SetActive (false);
				NormGuy.SetActive (false);
				Spear.SetActive (false);
				charScreen.SetActive (false);
			} else if (Type == "spear") {
				Archer.SetActive (false);
				Drag.SetActive (false);
				Wizard.SetActive (false);
				Norm.SetActive (false);
				NormGuy.SetActive (false);
				Spear.SetActive (true);
				charScreen.SetActive (false);
			} else if (Type == "drag") {
				Archer.SetActive (false);
				Drag.SetActive (true);
				Wizard.SetActive (false);
				Norm.SetActive (false);
				NormGuy.SetActive (false);
				Spear.SetActive (false);
				charScreen.SetActive (false);
			} else if (Type == "archer") {
				Archer.SetActive (true);
				Drag.SetActive (false);
				Wizard.SetActive (false);
				Norm.SetActive (false);
				NormGuy.SetActive (false);
				Spear.SetActive (false);
				charScreen.SetActive (false);
			} else if (Type == "norm") {
				Archer.SetActive (false);
				Drag.SetActive (false);
				Wizard.SetActive (false);
				Norm.SetActive (true);
				NormGuy.SetActive (false);
				Spear.SetActive (false);
				charScreen.SetActive (false);
			} else if (Type == "normguy") {
				Archer.SetActive (false);
				Drag.SetActive (false);
				Wizard.SetActive (false);
				Norm.SetActive (false);
				NormGuy.SetActive (true);
				Spear.SetActive (false);
				charScreen.SetActive (false);
			}

		}

		invincible = false;

	

		gameStart = true;
		enemies.Clear ();
	

	
	}

	public void easy ()
	{
		realDifficulty = 1;
		Difficulty = 3;
		isHard = false;
		hardEnemies.SetActive (false);
		diffScreen.SetActive (false);
		hasGameStarted = true;
		settingUpGame = false;
		
		areEnemiesMoving = false;
	}

	public void medium ()
	{
		realDifficulty = 2;
		Difficulty = 2;
		isHard = false;
		diffScreen.SetActive (false);
		hasGameStarted = true;
		settingUpGame = false;
		
		areEnemiesMoving = false;
	}

	public void hard ()
	{
		realDifficulty = 3;
		Difficulty = 2;
		isHard = true;
		pickups.SetActive (false);
		diffScreen.SetActive (false);
		hasGameStarted = true;
		settingUpGame = false;
		
		areEnemiesMoving = false;
	}

	public void closeBuyScreen ()
	{
		buyScreen.SetActive (false);
	}

	public void openBuyScreen ()
	{
		buyScreen.SetActive (true);
	}



	public void Controls ()
	{
		controlScreen.SetActive (true);
	}

	public void ContBack ()
	{
		controlScreen.SetActive (false);
	}


	public void LoadStartGame ()
	{

		if (System.IO.File.Exists ("Player1Health")) {
			Game.LoadGameData ();
			Game.NeedGameUpdate2 ();
		
			startScreen.SetActive (false);


			if (Type == "wizard") {
				wizard ();
			} else if (Type == "norm") {
				norm ();
			} else if (Type == "normguy") {
				normguy ();
			} else if (Type == "archer") {
				archer ();
			} else if (Type == "drag") {
				drag ();
			} else if (Type == "spear") {
				spear ();
			}


				
				

			if (realDifficulty == 1) {
				easy ();
			} else if (realDifficulty == 2) {
				medium ();
			} else if (realDifficulty == 3) {
				hard ();
			}

			playerCurrentHealth = Game.player1health;
			Player.setHealth ();

			Application.LoadLevel (currentLevel - 1);
			gameStart = true;
		}
	}

	public void SaveGame ()
	{

		Game.NeedGameUpdate ();
		Game.SaveGameData ();
		
	}

	private void replacePname1 (int contender, string contName)
	{
		plevel1 = contender;
		pname1 = contName;

	}

	private void replacePname2 (int contender, string contName)
	{
		plevel2 = contender;
		pname2 = contName;
		
	}

	private void replacePname3 (int contender, string contName)
	{
		plevel3 = contender;
		pname3 = contName;
		
	}

	private void replacePname4 (int contender, string contName)
	{
		plevel4 = contender;
		pname4 = contName;
		
	}
	public void UWON(){
		leadScreen.SetActive (true);
		leadText.text = "You found the cure and saved the world.";
	}
	private void replacePname5 (int contender, string contName)
	{
		plevel5 = contender;
		pname5 = contName;
		
	}




	public void wizard ()
	{
		Archer.SetActive (false);
		Drag.SetActive (false);
		Wizard.SetActive (true);
		Norm.SetActive (false);
		NormGuy.SetActive (false);
		Spear.SetActive (false);
		charScreen.SetActive (false);
		Type = "wizard";
		Player.lives = 5;
			diffScreen.SetActive (true);

	}

	public void norm ()
	{
		Type = "norm";
		Archer.SetActive (false);
		Drag.SetActive (false);
		Wizard.SetActive (false);
		Norm.SetActive (true);
		NormGuy.SetActive (false);
		Spear.SetActive (false);
		charScreen.SetActive (false);
	
			diffScreen.SetActive (true);

	}

	public void normguy ()
	{
		Type = "normguy";
		Archer.SetActive (false);
		Drag.SetActive (false);
		Wizard.SetActive (false);
		Norm.SetActive (false);
		NormGuy.SetActive (true);
		Spear.SetActive (false);
		charScreen.SetActive (false);

			diffScreen.SetActive (true);
		Player.speed = 30f;
		Player.maxSpeed = 5;

	}

	public void drag ()
	{
		Type = "drag";
		playerCurrentHealth = 100;
		Archer.SetActive (false);
		Drag.SetActive (true);
		Wizard.SetActive (false);
		Norm.SetActive (false);
		NormGuy.SetActive (false);
		Spear.SetActive (false);
		charScreen.SetActive (false);
	
			diffScreen.SetActive (true);

	}

	public void spear ()
	{
		Type = "spear";
		Archer.SetActive (false);
		Drag.SetActive (false);
		Wizard.SetActive (false);
		Norm.SetActive (false);
		NormGuy.SetActive (false);
		Spear.SetActive (true);
		charScreen.SetActive (false);

			diffScreen.SetActive (true);
		Player.jumpPower = 350f;

	}

	public void archer ()
	{
		Type = "archer";
		Archer.SetActive (true);
		Drag.SetActive (false);
		Wizard.SetActive (false);
		Norm.SetActive (false);
		NormGuy.SetActive (false);
		Spear.SetActive (false);

		charScreen.SetActive (false);
			diffScreen.SetActive (true);

	}







	private void OnLevelWasLoaded (int levelLoaded)
	{
		Debug.Log (currentLevel);
		currentLevel++;
		InitializeGame ();
	}

	void Update ()
	{
		if (areEnemiesMoving || settingUpGame) {
			return;
		}
		StartCoroutine (MoveEnemies ());
	}

	private IEnumerator MoveEnemies ()
	{


		yield return new WaitForSeconds (0.2f);

		foreach (Enemy enemy in enemies) {
			yield return new WaitForSeconds (enemy.moveTime);
		}


	}

	public void AddEnemyToList (Enemy enemy)
	{
		enemies.Add (enemy);
	}

	public void StartGame ()
	{
		if (System.IO.File.Exists ("canon")) {
			
			Game.LoadCanonData ();
			
			Game.NeedUpdate2 ();
			Debug.Log("We Made It 1");
		}
		startScreen.SetActive (false);
		charScreen.SetActive (true);
	
	}

	public void testing ()
	{
		Debug.Log ("Testing");
	}

	public void Gun ()
	{
		Buying = "Gun";
		Player.gun = true;

	}
	
	public void Nuke ()
	{
		Buying = "Nuke";
		if (Type == "spear") {
			Player.jumpPower = 700f;
		} else {
			Player.jumpPower = 450f;
		}
	
	}
	
	public void SkipLevel ()
	{
		Buying = "SkipLevel";
		if (Type == "normguy") {
			Player.speed = 50f;
			Player.maxSpeed = 8;
		} else {
			Player.speed = 30f;
			Player.maxSpeed = 5;
		}
	}

	public void GameOver ()
	{

		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);

		leadScreen.SetActive (true);


		Archer.SetActive (false);
		Drag.SetActive (false);
		Wizard.SetActive (false);
		Norm.SetActive (false);
		NormGuy.SetActive (false);
		Spear.SetActive (false);


		enabled = false;

		
	}
}
