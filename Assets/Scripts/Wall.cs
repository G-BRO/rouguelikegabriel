﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{

	public int hitPoints = 2;
	public Sprite damagedWallSprite;
	private SpriteRenderer spriteRenderer;
	private int secondsUntilDone = 20;

	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();

	}

	public void DamageWall (int damageReceived)
	{
		hitPoints -= damageReceived;
		spriteRenderer.sprite = damagedWallSprite;
		if (hitPoints <= 0) {
			int chanceToPower = Random.Range (1, 30);
			if (chanceToPower < 3) {
				Debug.Log ("invin1111111");
				//play invincible sound
				GameController.Instance.invincible = true;
				Debug.Log (GameController.Instance.invincible);
				Invoke ("Invin", secondsUntilDone);

			}
			gameObject.SetActive (false);

		}
	}

	private void OnTriggerEnter2D (Collider2D objectEnemyCollideWith)
	{
		if (objectEnemyCollideWith.tag == "bullet") {

			objectEnemyCollideWith.gameObject.SetActive (false);

		} else if (objectEnemyCollideWith.tag == "orb") {

		} else if (objectEnemyCollideWith.tag == "spear") {
			objectEnemyCollideWith.gameObject.SetActive (false);
		} else if (objectEnemyCollideWith.tag == "dragSpear") {
			objectEnemyCollideWith.gameObject.SetActive (false);
		} else if (objectEnemyCollideWith.tag == "greatArrow") {
			objectEnemyCollideWith.gameObject.SetActive (false);
		}
	}

	private void Invin ()
	{


		GameController.Instance.invincible = false;
		Debug.Log (GameController.Instance.invincible);
	}
	
}
