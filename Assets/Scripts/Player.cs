﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player : MovingObject
{

	public static Text healthText;
	public static Text storyText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;
	public AudioClip InvinSound;
	public Rigidbody2D bulletPrefab;
	public Rigidbody2D greatArrowPrefab;
	public Rigidbody2D canonPrefab;
	public Rigidbody2D orbPrefab;
	public Rigidbody2D spearPrefab;
	public Rigidbody2D dragSpearPrefab;
	public static bool gun;
	public static bool revive;
	public float fireDelay = .25f;
	float cooldownTimer = 0f;
	private bool storyStart = false;
	private int attackPower = 1;
	private static int playerHealth;
	private Animator animator;
	private int healthPerFruit = 5;
	private int healthPerSoda = 10;
	private int healthPerHealthPack = 20;
	private int secondsUntilNextLevel = 1;
	private int secondsUntilDone = 1;
	private bool buscreen = false;
	public static bool canon = false;

	protected override void Start ()
	{
		base.Start ();
		animator = GetComponent<Animator> ();
		//get the transform for this gameobject
		healthText = GameObject.Find ("Health Text").GetComponent<Text> ();
		storyText = GameObject.Find ("Story Text").GetComponent<Text> ();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Player 1 Health: " + playerHealth;


	}

	private void OnDisable ()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}

	public static void setHealth ()
	{
		playerHealth = Game.player1health;
		healthText.text = "Player 1 Health: " + playerHealth;
	}

	public static void setHealth2 ()
	{
		Game.player1health = playerHealth;
			

	}

	void Update ()
	{
	
		if (!GameController.Instance.isPlayerTurn && GameController.Instance.is2Player == false) {
			return;
		}
			
		if (!GameController.Instance.isPlayerTurn || GameController.Instance.is2PlayerTurn || !GameController.Instance.isPlayer1Turn && GameController.Instance.is2Player == true) {
			return;
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			playerHealth = playerHealth - 200;
		}
		if (Input.GetKeyDown (KeyCode.C)) {
			GameController.Instance.SaveGame ();
			Debug.Log(Application.persistentDataPath);
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			canon = false;
			Game.NeedUpdate();
			Game.SaveCanonData();

		}

		cooldownTimer -= Time.deltaTime;
		if (gun == true) {
			healthText.text = "Gun Activated";
			if (Input.GetKeyDown (KeyCode.T) && cooldownTimer <= 0) {
				animator.SetTrigger ("Up Shoot");
				GameController.Instance.faceShot = 1;
				//Shoot
		
				Vector2 playerPosition = rigidBody.position;
				if (canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}


				Debug.Log (rigidBody.position);
				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				if (GameController.Instance.is2Player == true) {
					Invoke ("ChangeTurn", secondsUntilDone);
					GameController.Instance.isPlayer1Turn = false;
				} else {
					GameController.Instance.isPlayerTurn = false;
				}
			} else if (Input.GetKeyDown (KeyCode.G) && cooldownTimer <= 0) {
				animator.SetTrigger ("Down Shoot");
				GameController.Instance.faceShot = 2;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}
			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				if (GameController.Instance.is2Player == true) {
					Invoke ("ChangeTurn", secondsUntilDone);
					GameController.Instance.isPlayer1Turn = false;
				} else {
					GameController.Instance.isPlayerTurn = false;
				}
			} else if (Input.GetKeyDown (KeyCode.F) && cooldownTimer <= 0) {
				animator.SetTrigger ("Left Shoot");
				GameController.Instance.faceShot = 3;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}
			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				if (GameController.Instance.is2Player == true) {
					Invoke ("ChangeTurn", secondsUntilDone);
					GameController.Instance.isPlayer1Turn = false;
				} else {
					GameController.Instance.isPlayerTurn = false;
				}
			} else if (Input.GetKeyDown (KeyCode.H) && cooldownTimer <= 0) {
				animator.SetTrigger ("Right Shoot");
				GameController.Instance.faceShot = 4;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}
			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				if (GameController.Instance.is2Player == true) {
					Invoke ("ChangeTurn", secondsUntilDone);
					GameController.Instance.isPlayer1Turn = false;
				} else {
					GameController.Instance.isPlayerTurn = false;
				}
			
			}

		}

		CheckIfGameOver ();
		int xAxis = 0;
		int yAxis = 0;



		if (xAxis != 0) {
			yAxis = 0;
		}
		if (GameController.Instance.Buying == "Gun") {
			playerHealth = playerHealth - 50;
			healthText.text = "Player 1 Health: " + playerHealth;

			GameController.Instance.Buying = "null";
		} else if (GameController.Instance.Buying == "Nuke") {
			playerHealth = playerHealth - 100;
			healthText.text = "Player 1 Health: " + playerHealth;
			GameController.Instance.Buying = "null";

		} else if (GameController.Instance.Buying == "SkipLevel") {
			playerHealth = playerHealth - 100;
			healthText.text = "Player 1 Health: " + playerHealth;
			GameController.Instance.Buying = "null";

		}
		if (Input.GetKeyDown (KeyCode.S)) {
			animator.SetTrigger ("Down Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 1");
			yAxis = (int)Input.GetAxisRaw ("Vertical 1");
		} else if (Input.GetKeyDown (KeyCode.W)) {
			animator.SetTrigger ("Up Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 1");
			yAxis = (int)Input.GetAxisRaw ("Vertical 1");
		} else if (Input.GetKeyDown (KeyCode.D)) {
			animator.SetTrigger ("Right Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 1");
			yAxis = (int)Input.GetAxisRaw ("Vertical 1");
		} else if (Input.GetKeyDown (KeyCode.A)) {
			
			animator.SetTrigger ("Left Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 1");
			yAxis = (int)Input.GetAxisRaw ("Vertical 1");
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			if (buscreen == false) {
				GameController.Instance.openBuyScreen ();
				buscreen = true;
			} else if (buscreen == true) {
				GameController.Instance.closeBuyScreen ();
				buscreen = false;
			}
		}
		if (xAxis != 0 || yAxis != 0) {
			playerHealth--;
			healthText.text = "Player 1 Health: " + playerHealth;
			SoundController.Instance.PlaySingle (movementSound1, movementSound2);
			Move<Wall> (xAxis, yAxis);
			if (playerHealth <= 10) {
				storyText.text = "You: This can't be the end. I am too young.";

			}
			if (storyStart == false) {
				if (GameController.Instance.realDifficulty == 1) {
					if (GameController.Instance.currentLevel == 1) {
						storyStart = true;
						storyText.text = "You: Where am I?";
						Invoke ("Story", 3);
					} else if (GameController.Instance.currentLevel == 2) {
						storyStart = true;
						storyText.text = "You: Wow another cave?";
					} else if (GameController.Instance.currentLevel == 3) {
						storyStart = true;
						storyText.text = "You: What the hell is that?";
				
					} else if (GameController.Instance.currentLevel == 4) {
						storyStart = true;
						storyText.text = "You: Eventually I will have to risk some health for a gun.";
					
					}else if (GameController.Instance.currentLevel == 5) {
						storyStart = true;
						storyText.text = "You: This is harder than expected.";
						
					} else if (GameController.Instance.currentLevel == 6) {
						storyStart = true;
						storyText.text = "You: I cant remember.";
						
					} else if (GameController.Instance.currentLevel == 7) {
						storyStart = true;
						storyText.text = "You: Wait I remember.";
						
					} else if (GameController.Instance.currentLevel == 8) {
						storyStart = true;
						storyText.text = "You: There was a group of people I was with.";
						
					} else if (GameController.Instance.currentLevel == 9) {
						storyStart = true;
						storyText.text = "You: We decided to search a cave.";
						
					} else if (GameController.Instance.currentLevel == 10) {
						storyStart = true;
						storyText.text = "You: Raymond said it was a bad idea.";
						
					} else if (GameController.Instance.currentLevel == 11) {
						storyStart = true;
						storyText.text = "You: Shit! Should of listened to Raymond.";
						
					} else if (GameController.Instance.currentLevel == 12) {
						storyStart = true;
						storyText.text = "You: It was the rumors.";
						
					} else if (GameController.Instance.currentLevel == 13) {
						storyStart = true;
						storyText.text = "You: They drived us crazy.";
						
					} else if (GameController.Instance.currentLevel == 14) {
						storyStart = true;
						storyText.text = "You: We needed it.";
						
					} else if (GameController.Instance.currentLevel == 15) {
						storyStart = true;
						storyText.text = "You: It was the only thing somone would want in a apocolyse.";
						
					} else if (GameController.Instance.currentLevel == 16) {
						storyStart = true;
						storyText.text = "You: I need it!";
						
					} else if (GameController.Instance.currentLevel == 17) {
						storyStart = true;
						storyText.text = "You: I remember it being rumored to be 100 caves down.";
						
					} else if (GameController.Instance.currentLevel == 18) {
						storyStart = true;
						storyText.text = "You: Shit! Well at least I know how far the surface is.";
						
					} else if (GameController.Instance.currentLevel == 19) {
						storyStart = true;
						storyText.text = "You: 82 more Levels to go!";
						
					} else if (GameController.Instance.currentLevel == 20) {
						storyStart = true;
						storyText.text = "You: Oh no! I remember. Talon has it.";
						
					} else if (GameController.Instance.currentLevel == 21) {
						storyStart = true;
						storyText.text = "You: That son of a bitch took it and knocked us out and ran.";
						
					} else if (GameController.Instance.currentLevel == 22) {
						storyStart = true;
						storyText.text = "You: I'm going to stop talking to myself till I get to the surface.";
						
					} else if (GameController.Instance.currentLevel == 99) {
						storyStart = true;
						storyText.text = "You: Almost There!";
						
					} else if (GameController.Instance.currentLevel == 100) {
						storyStart = true;
						storyText.text = "You: Finally I have the canon!";
						
					}else if (GameController.Instance.currentLevel == 101) {
						canon = true;
						Game.NeedUpdate();
						Game.SaveCanonData();
						playerHealth = -10;
						Player2.playerHealth = -10;
						
					}
				} else if (GameController.Instance.realDifficulty == 2 || GameController.Instance.realDifficulty == 3) {
					if (GameController.Instance.currentLevel == 1) {
						storyStart = true;
						storyText.text = "You: Where am I?";
						Invoke ("Story", 3);
					} else if (GameController.Instance.currentLevel == 2) {
						storyStart = true;
						storyText.text = "You: What the hell is that?";
					} else if (GameController.Instance.currentLevel == 3) {
						storyStart = true;
						storyText.text = "You: Eventually I will have to risk some health for a gun.";
						
					} else if (GameController.Instance.currentLevel == 4) {
						storyStart = true;
						storyText.text = "You: This is harder than expected.";
						
					} else if (GameController.Instance.currentLevel == 5) {
						storyStart = true;
						storyText.text = "You: I cant remember.";
						
					} else if (GameController.Instance.currentLevel == 6) {
						storyStart = true;
						storyText.text = "You: Wait I remember.";
						
					} else if (GameController.Instance.currentLevel == 7) {
						storyStart = true;
						storyText.text = "You: There was a group of people I was with.";
						
					} else if (GameController.Instance.currentLevel == 8) {
						storyStart = true;
						storyText.text = "You: We decided to search a cave.";
						
					} else if (GameController.Instance.currentLevel == 9) {
						storyStart = true;
						storyText.text = "You: Raymond said it was a bad idea.";
						
					} else if (GameController.Instance.currentLevel == 10) {
						storyStart = true;
						storyText.text = "You: Shit! Should of listened to Raymond.";
						
					} else if (GameController.Instance.currentLevel == 11) {
						storyStart = true;
						storyText.text = "You: It was the rumors.";
						
					} else if (GameController.Instance.currentLevel == 12) {
						storyStart = true;
						storyText.text = "You: They drived us crazy.";
						
					} else if (GameController.Instance.currentLevel == 13) {
						storyStart = true;
						storyText.text = "You: We needed it.";
						
					} else if (GameController.Instance.currentLevel == 14) {
						storyStart = true;
						storyText.text = "You: It was the only thing somone would want in a apocolyse.";
						
					} else if (GameController.Instance.currentLevel == 15) {
						storyStart = true;
						storyText.text = "You: I need it!";
						
					} else if (GameController.Instance.currentLevel == 16) {
						storyStart = true;
						storyText.text = "You: I remember it being rumored to be 100 caves down.";
						
					} else if (GameController.Instance.currentLevel == 17) {
						storyStart = true;
						storyText.text = "You: Shit! Well at least I know how far the surface is.";
						
					} else if (GameController.Instance.currentLevel == 18) {
						storyStart = true;
						storyText.text = "You: 82 more Levels to go!";
						
					} else if (GameController.Instance.currentLevel == 19) {
						storyStart = true;
						storyText.text = "You: Oh no! I remember. Talon has it.";
						
					} else if (GameController.Instance.currentLevel == 20) {
						storyStart = true;
						storyText.text = "You: That son of a bitch took it and knocked us out and ran.";
						
					} else if (GameController.Instance.currentLevel == 21) {
						storyStart = true;
						storyText.text = "You: I'm going to stop talking to myself till I get to the surface.";
						
					} else if (GameController.Instance.currentLevel == 99) {
						storyStart = true;
						storyText.text = "You: Almost There!";
						
					} else if (GameController.Instance.currentLevel == 100) {
						storyStart = true;
						storyText.text = "You: Finally I have the canon.";
						
					}else if (GameController.Instance.currentLevel == 101) {
						canon = true;
						Game.NeedUpdate();
						Game.SaveCanonData();
						playerHealth = -10;
						Player2.playerHealth = -10;
						
					}
				}
			}

			if (GameController.Instance.is2Player == true) {
				if (GameController.Instance.Type == "normguy") {
					int chanceToPower = Random.Range (1, 30);
					if (chanceToPower < 15) {
					
					} else {
					
						Invoke ("ChangeTurn", secondsUntilDone);
						GameController.Instance.isPlayer1Turn = false;
					}
				} else {
					
					Invoke ("ChangeTurn", secondsUntilDone);
					GameController.Instance.isPlayer1Turn = false;
				}


			} else {
				if (GameController.Instance.Type == "normguy") {
					int chanceToPower = Random.Range (1, 30);
					if (chanceToPower < 15) {

					} else {

						GameController.Instance.isPlayerTurn = false;
					}
				} else {
					

					GameController.Instance.isPlayerTurn = false;
				}
			}

		

		
		}
	}

	private void Story ()
	{
		storyText.text = "You: I need to get out?";
	}

	private void ChangeTurn ()
	{
		GameController.Instance.is2PlayerTurn = true;
	}

	private void OnTriggerEnter2D (Collider2D objectPlayerCollideWith)
	{
		if (objectPlayerCollideWith.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		} else if (objectPlayerCollideWith.tag == "Fruit") {
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Player 1 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (fruitSound1, fruitSound2);
		} else if (objectPlayerCollideWith.tag == "Soda") {
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + " Health\n" + "Player 1 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (sodaSound1, sodaSound2);
		} else if (objectPlayerCollideWith.tag == "Health Pack") {
			playerHealth += healthPerHealthPack;
			healthText.text = "+" + healthPerHealthPack + " Health\n" + "Player 1 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (sodaSound1, sodaSound2);
		}

	}

	private void LoadNewLevel ()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	protected override void HandleCollision<T> (T component)
	{
		if (GameController.Instance.Type == "spear") {
			attackPower = 2;
		}
		Wall wall = component as Wall;
		if (Input.GetKeyDown (KeyCode.S)) {

			animator.ResetTrigger ("Down Walk");
				
			animator.SetTrigger ("Down Attack");

		} else if (Input.GetKeyDown (KeyCode.W)) {
			animator.ResetTrigger ("Up Walk");
			animator.SetTrigger ("Up Attack");
		} else if (Input.GetKeyDown (KeyCode.D)) {
			animator.ResetTrigger ("Right Walk");
			animator.SetTrigger ("Right Attack");
		} else if (Input.GetKeyDown (KeyCode.A)) {
			animator.ResetTrigger ("Left Walk");
			animator.SetTrigger ("Left Attack");
		}
		SoundController.Instance.PlaySingle (chopSound1, chopSound2);
		wall.DamageWall (attackPower);
	}

	public void TakeDamage (int damageReceived)
	{
		if (GameController.Instance.invincible == true) {
			healthText.text = "Invincible Activated";
			SoundController.Instance.NotRandomPlaySingle (InvinSound);
		} else if (GameController.Instance.invincible == false) {
			playerHealth -= damageReceived;
			healthText.text = "-" + damageReceived + " Health\n" + "Player 1 Health: " + playerHealth;
			
		}

	}

	private void CheckIfGameOver ()
	{
		if (playerHealth <= 0) {
			if (revive) {
				playerHealth = 50;
				revive = false;

			} else if (!revive) {
				GameController.Instance.GameOver ();
			}

		}
	}
}
