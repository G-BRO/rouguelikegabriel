﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	public Rigidbody2D bulletPrefab;
	public float speed = 20f;
	// Use this for initialization
	void Start ()
	{
		if (Player.gun == true) {
			if (GameController.Instance.faceShot == 1) {

				bulletPrefab.rotation = 90;
				bulletPrefab.velocity = transform.up * speed;
			} else if (GameController.Instance.faceShot == 2) {
				bulletPrefab.rotation = 270;
			
				bulletPrefab.velocity = -transform.up * speed;
			} else if (GameController.Instance.faceShot == 3) {
				bulletPrefab.rotation = 180;
				bulletPrefab.velocity = -transform.right * speed;

			} else if (GameController.Instance.faceShot == 4) {


				bulletPrefab.velocity = transform.right * speed;
			}
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
	
		if (bulletPrefab.position.x > 10 || bulletPrefab.position.y > 10 || bulletPrefab.position.x < -1 || bulletPrefab.position.y < -1) {
			gameObject.SetActive (false);
		}
	}
}
