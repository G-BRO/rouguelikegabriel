﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player2 : MovingObject
{

	public static Text healthText;
	public static Text storyText;

	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;
	public AudioClip InvinSound;
	public Rigidbody2D bulletPrefab;
	public Rigidbody2D orbPrefab;
	public Rigidbody2D spearPrefab;
	public Rigidbody2D dragSpearPrefab;
	public Rigidbody2D greatArrowPrefab;
	public Rigidbody2D canonPrefab;
	public static bool gun;
	public static bool revive;
	public float fireDelay = .25f;
	float cooldownTimer = 0f;
	private int attackPower = 1;
	public static int playerHealth;
	private Animator animator;
	private int healthPerFruit = 5;
	private int healthPerSoda = 10;
	private int healthPerHealthPack = 20;
	private int secondsUntilNextLevel = 1;

	protected override void Start ()
	{

		base.Start ();
		animator = GetComponent<Animator> ();
		healthText = GameObject.Find ("Health Text").GetComponent<Text> ();
		storyText = GameObject.Find ("Story Text").GetComponent<Text> ();
		playerHealth = GameController.Instance.player2CurrentHealth;
		healthText.text = "Player 2 Health: " + playerHealth;


	}
	
	private void OnDisable ()
	{
		GameController.Instance.player2CurrentHealth = playerHealth;
	}

	public static void setHealth ()
	{
		playerHealth = Game.player2health;
		healthText.text = "Player 2 Health: " + playerHealth;
	}
	
	public static void setHealth2 ()
	{
		Game.player2health = playerHealth;
		
		
	}

	void Update ()
	{
		
		if (!GameController.Instance.isPlayerTurn || !GameController.Instance.is2PlayerTurn) {
			return;
		}
		
		
		cooldownTimer -= Time.deltaTime;
		if (Player.gun == true) {
			healthText.text = "Gun Activated";
			if (Input.GetKeyDown (KeyCode.I) && cooldownTimer <= 0) {
				animator.SetTrigger ("Up Shoot");
				GameController.Instance.faceShot = 1;
				//Shoot
		
				Vector2 playerPosition = rigidBody.position;

				if (Player.canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type2 == "norm" || GameController.Instance.Type2 == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}

		
				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				GameController.Instance.is2PlayerTurn = false;
				GameController.Instance.isPlayerTurn = false;
			} else if (Input.GetKeyDown (KeyCode.K) && cooldownTimer <= 0) {
				animator.SetTrigger ("Down Shoot");
				GameController.Instance.faceShot = 2;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (Player.canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}

			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				GameController.Instance.is2PlayerTurn = false;
				GameController.Instance.isPlayerTurn = false;
			} else if (Input.GetKeyDown (KeyCode.J) && cooldownTimer <= 0) {
				animator.SetTrigger ("Left Shoot");
				GameController.Instance.faceShot = 3;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (Player.canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}

			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				GameController.Instance.is2PlayerTurn = false;
				GameController.Instance.isPlayerTurn = false;
			} else if (Input.GetKeyDown (KeyCode.L) && cooldownTimer <= 0) {
				animator.SetTrigger ("Right Shoot");
				GameController.Instance.faceShot = 4;
				//Shoot
			
				Vector2 playerPosition = rigidBody.position;
			
				if (Player.canon == true) {
					Instantiate (canonPrefab, playerPosition, Quaternion.identity);
				}
				else if (GameController.Instance.Type == "norm" || GameController.Instance.Type == "normguy") {
					Instantiate (bulletPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "wizard") {
					Instantiate (orbPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "drag") {
					Instantiate (dragSpearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "spear") {
					Instantiate (spearPrefab, playerPosition, Quaternion.identity);
				} else if (GameController.Instance.Type2 == "archer") {
					Instantiate (greatArrowPrefab, playerPosition, Quaternion.identity);
				}

			

				Debug.Log ("Pew");
				cooldownTimer = fireDelay;
				GameController.Instance.is2PlayerTurn = false;
				GameController.Instance.isPlayerTurn = false;
			}
		}
		
		
		CheckIfGameOver ();
		int xAxis = 0;
		int yAxis = 0;
		


		if (GameController.Instance.Buying == "Gun") {
			playerHealth = playerHealth - 20;
			healthText.text = "Player 2 Health: " + playerHealth;
			
			GameController.Instance.Buying = "null";
		} else if (GameController.Instance.Buying == "Nuke") {
			playerHealth = playerHealth - 30;
			healthText.text = "Player 2 Health: " + playerHealth;
			GameController.Instance.Buying = "null";
			
		} else if (GameController.Instance.Buying == "SkipLevel") {
			playerHealth = playerHealth - 40;
			healthText.text = "Player 2 Health: " + playerHealth;
			GameController.Instance.Buying = "null";
			
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			animator.SetTrigger ("Down Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 2");
			yAxis = (int)Input.GetAxisRaw ("Vertical 2");
		} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			animator.SetTrigger ("Up Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 2");
			yAxis = (int)Input.GetAxisRaw ("Vertical 2");
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			animator.SetTrigger ("Right Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 2");
			yAxis = (int)Input.GetAxisRaw ("Vertical 2");
		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			
			animator.SetTrigger ("Left Walk");
			xAxis = (int)Input.GetAxisRaw ("Horizontal 2");
			yAxis = (int)Input.GetAxisRaw ("Vertical 2");
		}


		if (xAxis != 0) {
			yAxis = 0;
		}
		
		
		if (xAxis != 0 || yAxis != 0) {
			playerHealth--;
			healthText.text = "Player 2 Health: " + playerHealth;
			SoundController.Instance.PlaySingle (movementSound1, movementSound2);
			Move<Wall> (xAxis, yAxis);
			if (GameController.Instance.Type2 == "normguy") {
				int chanceToPower = Random.Range (1, 30);

				if (chanceToPower < 15) {
				
				} else {
				
					GameController.Instance.is2PlayerTurn = false;
					GameController.Instance.isPlayerTurn = false;
				}
			} else {
				
				GameController.Instance.is2PlayerTurn = false;
				GameController.Instance.isPlayerTurn = false;
			}

		
		}
	}

	private void OnTriggerEnter2D (Collider2D objectPlayerCollideWith)
	{
		if (objectPlayerCollideWith.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		} else if (objectPlayerCollideWith.tag == "Fruit") {
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Player 2 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (fruitSound1, fruitSound2);
		} else if (objectPlayerCollideWith.tag == "Soda") {
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + " Health\n" + "Player 2 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (sodaSound1, sodaSound2);
		} else if (objectPlayerCollideWith.tag == "Health Pack") {
			playerHealth += healthPerHealthPack;
			healthText.text = "+" + healthPerHealthPack + " Health\n" + "Player 2 Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (sodaSound1, sodaSound2);
		}
		
	}

	private void LoadNewLevel ()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
	
	protected override void HandleCollision<T> (T component)
	{
		if (GameController.Instance.Type2 == "spear") {
			attackPower = 2;
		}
		Wall wall = component as Wall;
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			
			animator.ResetTrigger ("Down Walk");
			
			animator.SetTrigger ("Down Attack");
			
		} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			animator.ResetTrigger ("Up Walk");
			animator.SetTrigger ("Up Attack");
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			animator.ResetTrigger ("Right Walk");
			animator.SetTrigger ("Right Attack");
		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			animator.ResetTrigger ("Left Walk");
			animator.SetTrigger ("Left Attack");
		}
		SoundController.Instance.PlaySingle (chopSound1, chopSound2);
		wall.DamageWall (attackPower);
	}

	public void TakeDamage (int damageReceived)
	{
		if (GameController.Instance.invincible == true) {
			//play invincible sound
		} else if (GameController.Instance.invincible == false) {
			playerHealth -= damageReceived;
			healthText.text = "-" + damageReceived + " Health\n" + "Player 2 Health: " + playerHealth;

		}
		
	}
	
	private void CheckIfGameOver ()
	{
		if (playerHealth <= 0 && GameController.Instance.is2Player == true) {
			if (revive) {
				playerHealth = 50;
				revive = false;
				
			} else if (!revive) {
				GameController.Instance.GameOver ();
			}
			
		}

	}
}
